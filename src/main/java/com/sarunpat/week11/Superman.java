package com.sarunpat.week11;

public class Superman extends Animal implements Flyable,Walkable{
    public Superman(String name) {
        super(name,2);
    }

    @Override
    public void sleep() {
        System.out.println(this.toString()+" sleep. ");
    }

    @Override
    public void eat() {
        System.out.println(this+" eat. ");
    }

    public String toString(){
        return "Superman ("+this.getName()+")";
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString()+" takeoff. ");
    }

    @Override
    public void fly() {
        System.out.println(this.toString()+" fly. ");
    }

    @Override
    public void landing() {
        System.out.println(this.toString()+" landing. ");
    }
    
    @Override
    public void walk() {
        System.out.println(this.toString()+" walk. ");
    }

    @Override
    public void run() {
        System.out.println(this.toString()+" run. ");
    }

}
