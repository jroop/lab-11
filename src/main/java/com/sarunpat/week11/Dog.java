package com.sarunpat.week11;

public class Dog extends Animal implements Walkable{
    public Dog(String name) {
        super(name,4);
    }

    @Override
    public void sleep() {
        System.out.println(this.toString().toString()+" sleep. ");
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+" eat. ");
    }

    public String toString(){
        return "Dog ("+this.getName()+")";
    }

    @Override
    public void walk() {
        System.out.println(this.toString()+" walk. ");
    }

    @Override
    public void run() {
        System.out.println(this.toString()+" run. ");
    }
}
